# Homework 4 #

See [week14/HW4.pdf](https://bitbucket.org/skeoop/oop/src/master/week14/HW4.pdf) for problems.  
This repository contains sample code
for creating Student objects used in the homework problems.